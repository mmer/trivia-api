//En base a la api Open Trivia (https://opentdb.com/api_config.php), 
//vamos a desarrollar un trivial con la siguiente url 'https://opentdb.com/api.php?amount=10'. 
//Esta api nos devolverá una serie de preguntas con sus respuestas, 
//tanto erroneas como correctas. 

//La idea es hacer un juego en el que el usuario introduzca en un input las 
//caracteristicas del Trivial y que al darle al 'Start Game' le salgan las 
//preguntas de la api para que pueda comenzar el juego. Una vez las responda todas, 
//le mostraremos al usuario el resultado.

//Ten en cuenta que hay dos tipos de preguntas. Aquellas con 3 respuestas erroneas
// y una correcta y aquellas con respuesta verdadero / falso.

const section$$ = document.querySelector('section');
const questionAnswered = [];
const play = async () => {

    reset();

    const numberQ = document.getElementById('numberQ').value;
    const url$$ = 'https://opentdb.com/api.php?amount=' + numberQ + '&type=multiple';

    const res = await fetch(url$$).then(res => res.json());

    questions = res.results;
    console.log(questions);
    for (i = 0; i < questions.length; i++) {
        createQuestions(questions[i], i);
    }

    const check$$ = document.createElement('button');
    check$$.innerHTML = 'Check!';
    check$$.classList.add('btn-submit');
    check$$.classList.add('check')
    check$$.addEventListener('click', checkgame);

    section$$.appendChild(check$$);
}

document.getElementById('sent-number').addEventListener('click', play);

const reset = () => {
    const questionAnswered = [];
    section$$.innerHTML = ' ';
}

function createQuestions(question, i) {

    //para cada pregunta creamos un articulo con un h1 diciendo la categoria, un h2 diciendo el numero de la pregunta
    //y un div con cuatro botones
    article$$ = document.createElement('article');
    h4$$ = document.createElement('h4');
    h4$$.innerHTML = ('Question number ' + (i + 1));
    h1$$ = document.createElement('h1');
    h1$$.innerHTML = (question.category);
    h2$$ = document.createElement('h2');
    h2$$.innerHTML = (question.question);

    div$$ = createAnswer([question.correct_answer, ...question.incorrect_answers], i, question.correct_answer);
    console.log(question.correct_answer);

    article$$.appendChild(h4$$);
    article$$.appendChild(h1$$);
    article$$.appendChild(h2$$);
    article$$.appendChild(div$$);

    section$$.appendChild(article$$);
}

function checkAnswer(answer, i, correct) {
    if (answer === correct) {
        questionAnswered[i] = true;
    } else {
        questionAnswered[i] = false;
    }
    console.log(questionAnswered);
}

const shuffleArray = (array) => {
    array.sort(() => Math.random() - 0.5);
};

function createAnswer(answers, i, correct) {
    div$$ = document.createElement('div');
    shuffleArray(answers);

    for (j = 0; j < answers.length; j++) {

        const button$$ = document.createElement('button');
        button$$.innerHTML = (answers[j]);
        button$$.addEventListener('click', function () {
            this.classList.add('clicked');
            checkAnswer(this.innerHTML, i, correct);
        })
        div$$.appendChild(button$$);
    }
    return div$$;
}

const checkgame = () => {
    cont = 0;

    for (i = 0; i < questionAnswered.length; i++) {
        if (questionAnswered[i] === true) {
            cont++;
        }
    }

    if (cont / questionAnswered.length > 0.5) {
        p$$ = document.createElement('p');
        p$$.classList.add('alert');
        p$$.innerHTML = 'You have ' + cont + " correct answers. Congrats!"
        document.body.appendChild(p$$)
    } else {
        p$$ = document.createElement('p');
        p$$.classList.add('alert');
        p$$.innerHTML = 'You have ' + cont + " correct answers. Try again!"
        document.body.appendChild(p$$)
    }
    p$$.addEventListener('click', function () {
        p$$.classList.add('display_none');
    })
}